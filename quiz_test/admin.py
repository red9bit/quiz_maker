from django.contrib import admin, messages
from django.db.models import F
from django.utils.html import format_html

from .models import Category, QuizTest, Question


def change_price_per_unit(modeladmin, request, queryset):
    queryset.update(price_per_unit=F('price_per_unit')+10)
    modeladmin.message_user(request, 'Successfully updated price_per_unit', messages.ERROR)


def change_question_title(modeladmin, request, queryset):
    try:
        queryset.update(tejkdshxt='Hello World')
    except Exception as e:
        modeladmin.message_user(request, e, messages.ERROR)
    else:
        modeladmin.message_user(request, 'Successfully updated price_per_unit', messages.INFO)


change_price_per_unit.short_description = 'Change price'
change_question_title.short_description = 'Change Text'


class QuestionInline(admin.TabularInline):
    model = Question
    extra = 4
    max_num = 4


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['id', 'text', 'sort', 'quiz_test_name']
    list_editable = ['text']
    list_filter = ['quiz_test']
    actions = [change_question_title]

    def quiz_test_name(self, obj):
        return obj.quiz_test.title
    quiz_test_name.short_description = 'Quiz Test Title'


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['slug', 'title', 'is_enable']
    list_filter = ['category_type']
    prepopulated_fields = {'slug': ('title',)}

    def get_prepopulated_fields(self, request, obj=None):
        return super(CategoryAdmin, self).get_prepopulated_fields(request, obj)

    def show_image(self, obj):
        tag = f'''<img src="{obj.image.url}" alt="Not Found" width="100" height="120">'''
        return format_html(tag)


class QuizTestAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'description', 'price_per_unit', 'count_of_categories', 'creator_id', 'test']
    # list_editable = ['title']
    ordering = ['price_per_unit', '-title']
    list_filter = ['is_enable']
    search_fields = ['title', 'creator__username']
    actions = [change_price_per_unit]
    inlines = [QuestionInline]

    def get_readonly_fields(self, request, obj=None):
        if request.user.username == 'red9bit':
            return []
        return self.readonly_fields

    def has_add_permission(self, request):
        if '9' not in request.user.username:
            return False
        return super(QuizTestAdmin, self).has_add_permission(request)
    
    def get_queryset(self, request):
        self.request = request
        return super(QuizTestAdmin, self).get_queryset(request)

    def count_of_categories(self, obj):
        return obj.categories.count()
    count_of_categories.short_description = 'Testtttttttttttttttt'

    def test(self, obj):
        return True if self.request.user.username == 'red9bit' else False
    test.short_description = 'USER'
    test.boolean = True

    @staticmethod
    def creator_id(obj):
        return obj.creator.id


admin.site.register(Category, CategoryAdmin)
admin.site.register(QuizTest, QuizTestAdmin)
admin.site.register(Question, QuestionAdmin)
