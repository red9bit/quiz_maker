from django.apps import AppConfig
from django.utils.translation import gettext as _


class QuizTestConfig(AppConfig):
    name = 'quiz_test'
    verbose_name = _('Quiz Test')
    verbose_name_plural = _('Quiz Tests')
