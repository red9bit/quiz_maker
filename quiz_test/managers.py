from django.db import models


class CategoryEnableObjects(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_enable=True)


class QuizTestEnableObjects(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_enable=True)
