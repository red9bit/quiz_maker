# Generated by Django 2.2.15 on 2020-08-16 05:46

from django.db import migrations, models
from django.db.models import Max, Count


def remove_duplicated_slugs(apps, schema_editor):
    category_model = apps.get_model('quiz_test', 'Category')
    valid_ids = list(
            category_model.objects.values(
                'slug'
            ).annotate(
                max_id=Max('id')
            ).values_list('max_id', flat=True)
    )

    category_model.objects.exclude(id__in=valid_ids).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('quiz_test', '0002_category_slug'),
    ]

    operations = [
        migrations.RunPython(remove_duplicated_slugs),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Slug'),
        ),
    ]
