# Generated by Django 2.2.15 on 2020-08-16 07:48

from django.db import migrations, models
import quiz_test.models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz_test', '0005_auto_20200816_0714'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=quiz_test.models.custom_upload_rename, verbose_name='Image'),
        ),
    ]
