import logging

from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from utils.utils import validate_between_1_to_10

from .managers import QuizTestEnableObjects, CategoryEnableObjects

logger = logging.getLogger('models')


def custom_upload_rename(instance, filename):
    return f'{instance.slug}/{filename}'


class Category(models.Model):
    CATEGORY_PRIMARY = 1
    CATEGORY_SECONDARY = 2
    CATEGORY_CHOICES = [
        (CATEGORY_PRIMARY, _('Primary Category')),
        (CATEGORY_SECONDARY, _('Secondary Category'))
    ]

    image = models.ImageField(verbose_name=_('Image'), upload_to=custom_upload_rename, null=True, blank=True)
    title = models.CharField(verbose_name=_('Title'), max_length=30)
    category_type = models.PositiveSmallIntegerField(verbose_name=_('Category Type'), choices=CATEGORY_CHOICES)
    slug = models.SlugField(verbose_name=_('Slug'), unique=True)
    is_enable = models.BooleanField(verbose_name=_('Is Enable'), default=True)
    view = models.IntegerField(default=0, validators=[validate_between_1_to_10])

    objects = models.Manager()
    enable_objects = CategoryEnableObjects()

    class Meta:
        db_table = 'categories'
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __init__(self, *args, **kwargs):
        super(Category, self).__init__(*args, **kwargs)
        self._is_enable = self.is_enable

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.is_enable != self._is_enable:
            print(True if self.is_enable else False)
        logger.debug('Debug')
        logger.error('Error')
        super(Category, self).save(*args, **kwargs)


class QuizTest(models.Model):
    created_time = models.DateTimeField(auto_now_add=True, verbose_name=_('created time'))
    updated_time = models.DateTimeField(auto_now=True, verbose_name=_('updated time'))
    creator = models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='quiz_tests',
                                on_delete=models.CASCADE, verbose_name=_('creator'))
    categories = models.ManyToManyField(to=Category, verbose_name=_('categories'))

    title = models.CharField(max_length=50, verbose_name=_('title'))
    description = models.TextField(blank=True, verbose_name=_('description'))
    price_per_unit = models.IntegerField(verbose_name=_('price per unit'))
    is_enable = models.BooleanField(default=False, verbose_name=_('enable status'))

    objects = models.Manager()
    enable_objects = QuizTestEnableObjects()

    class Meta:
        verbose_name = _('Quiz Test')
        verbose_name_plural = _('Quiz Tests')
        db_table = 'quiz_tests'

    def __str__(self):
        return self.title

    # def count_of_questions(self):
    #    return self.questions.all().count()


class Question(models.Model):
    quiz_test = models.ForeignKey(QuizTest, related_name='questions', on_delete=models.CASCADE, verbose_name=_('test'))
    sort = models.PositiveSmallIntegerField(default=0, verbose_name=_('sort'))
    text = models.CharField(max_length=120, verbose_name=_('text'))
    created_time = models.DateTimeField(auto_now_add=True, verbose_name=_('created time'))
    updated_time = models.DateTimeField(auto_now=True, verbose_name=_('updated time'))

    class Meta:
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')
        ordering = ['sort', 'text']

    def __str__(self):
        return f'{self.quiz_test.title} - {self.text}'

    # def count_of_choices(self):
    #    return self.choices.all().count()
