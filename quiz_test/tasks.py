from celery import shared_task
from celery.schedules import crontab
from celery.task import periodic_task


@periodic_task(name='hello_world', run_every=crontab(hour=16, minute=33))
def hello_world():
    print('Hello World')
