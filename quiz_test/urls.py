from django.urls import path

from .views import hello_world, bye_world, view_404_error, test_post, LoginView, test_for

urlpatterns = [
    path('', hello_world, name='hello_view'),
    path('test-for', test_for),
    path('login', LoginView.as_view(), name='login_view'),
    path('bye', bye_world),
    path('404', view_404_error),
    path('post', test_post),
]
