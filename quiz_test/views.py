from time import sleep

from django.contrib.auth import login, authenticate
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, Http404
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.contrib.auth.models import User
from django.views import View

from quiz_test.forms import LoginForm
from quiz_test.models import Category

from .tasks import hello_world as hello_func


@require_http_methods(['POST'])
def hello_world(request):
    context = {
        'text': 'This is a test message',
        'nasted': {
            'title': 'Hello'
        }
    }
    return render(request, 'quiz_test/index.html', context)


def test_for(request):
    categories = Category.objects.all()
    # categories = [Category.objects.get(pk=8)]
    return render(request, 'quiz_test/index.html', {'categories': categories})


@csrf_exempt
def test_post(request):
    a = 9
    return HttpResponse()


def bye_world(request):
    hello_func()
    hello_func.apply_async([])
    # content = '<html><head></head><body>Bye World</body></html>'
    # return redirect('http://127.0.0.1:8000/quiz_test')
    # return redirect('hello_view')
    return JsonResponse({'status': '200', 'msg': 'Hello World'})


def view_404_error(request):
    raise Http404('Not Found - this is a test')


class LoginView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        login_form = LoginForm()
        return render(request, 'quiz_test/login.html', {'form': login_form})

    def post(self, request):
        login_form = LoginForm(request.POST)
        res_data = {
            'status': 'Failed'
        }
        if login_form.is_valid():
            username = login_form.cleaned_data.get('username')
            password = login_form.cleaned_data.get('password')
            user = authenticate(request=request, username=username, password=password)
            if user:
                login(request, user)
                res_data.update(status='succeeded')
        return JsonResponse(res_data)
