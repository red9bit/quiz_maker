from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_between_1_to_10(value):
    if value < 0 or value > 10:
        raise ValidationError(_('Please enter a value between 1 to 10'))
